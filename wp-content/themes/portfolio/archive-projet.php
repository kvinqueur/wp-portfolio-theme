<?php get_header(); ?>
<div class="projets">
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <article class="projet">
                <?php if (has_post_thumbnail()) {
                the_post_thumbnail('thumbnail');
                }  ?>
                <h1 class="title">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h1>
                <div class="category">
                    <?php the_terms($post->ID, 'type', 'Type : '); ?>
                </div>
                <div class="content">
                    <?php the_content(); ?>
                </div>
                <div class="tags">
                    <?php the_terms($post->ID, 'couleur', 'Couleur : '); ?>
                </div>
            </article>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<?php get_footer() ?>
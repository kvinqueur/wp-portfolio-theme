<?php
/**
 * On active les miniatures
 */
add_theme_support('post-thumbnails');

/**
 * On active les formats de Post (Post Formats)
 */
function pf_theme_support()
{
    add_theme_support('post-formats', array('aside', 'gallery'));
}

add_action('after_setup_theme', 'pf_theme_support');

add_action('init', 'my_custom_init');

function my_custom_init()
{
    /*
     * On crée le Custom Post (Projet)
     */
    register_post_type(
        'project',
        array(
            'label' => 'Projects',
            'labels' => array(
                'name' => 'Projects',
                'singular_name' => 'Projet',
                'all_items' => 'All projects',
                'add_new_item' => 'Add project',
                'edit_item' => 'Edit project',
                'new_item' => 'New project',
                'view_item' => 'View project',
                'search_items' => 'Search projects',
                'not_found' => 'No project found',
                'not_found_in_trash' => 'No project in trash'
            ),
            'public' => true,
            'capability_type' => 'post',
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
            'has_archive' => true
        )
    );

    /**
     * On crée la catégorie "Type de projet"
     */

    register_taxonomy(
        'type',
        'projet',
        array(
            'label' => 'Types',
            'labels' => array(
                'name' => 'Types',
                'singular_name' => 'Type',
                'all_items' => 'Tous les types',
                'edit_item' => 'Éditer le type',
                'view_item' => 'Voir le type',
                'update_item' => 'Mettre à jour le type',
                'add_new_item' => 'Ajouter un type',
                'new_item_name' => 'Nouveau type',
                'search_items' => 'Rechercher parmi les types',
                'popular_items' => 'Types les plus utilisés'
            ),
            'hierarchical' => true
        )
    );

    /**
     * On crée le tag Couleur
     */

    register_taxonomy(
        'couleur',
        'projet',
        array(
            'label' => 'Couleurs',
            'labels' => array(
                'name' => 'Couleurs',
                'singular_name' => 'Couleur',
                'all_items' => 'Toutes les couleurs',
                'edit_item' => 'Éditer la couleur',
                'view_item' => 'Voir la couleur',
                'update_item' => 'Mettre à jour la couleur',
                'add_new_item' => 'Ajouter une couleur',
                'new_item_name' => 'Nouvelle couleur',
                'search_items' => 'Rechercher parmi les couleurs',
                'popular_items' => 'Couleurs les plus utilisées'
            ),
            'hierarchical' => false
        )
    );

    /**
     * On charge notre catégorie et tag dans le custom Post Projet
     */
    register_taxonomy_for_object_type('type', 'projet');
    register_taxonomy_for_object_type('couleur', 'projet');

    /**
     * Activer les widgets
     */
    if (function_exists('register_sidebar')) {
        register_sidebar(array(
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4 class="section">',
            'after_title' => '</h4>',
        ));
    }
}

/**
 * Add stylesheets
 */
function pf_enqueue_stylesheets() {
    /**
     * Composer vendor
     */
    $vendor_directory = get_template_directory_uri() . '/vendor/';
    /**
     * Stylesheets directory
     */
    $css_directory = get_template_directory_uri() . '/css/';
    wp_register_style('bootstrap-style', $vendor_directory . 'twbs/bootstrap/dist/css/bootstrap.css', 'jquery', '3.1.1', 'all');
    wp_register_style('font-awesome', $vendor_directory . 'fortawesome/font-awesome/css/font-awesome.min.css');
    wp_register_style('freelancer-style', $css_directory . 'freelancer.min.css');
    wp_enqueue_style('bootstrap-style');
    wp_enqueue_style('font-awesome');
    wp_enqueue_style('freelancer-style');
}
add_action('wp_enqueue_scripts', 'pf_enqueue_stylesheets');


/**
 * Add scripts
 */
function pf_enqueue_scripts() {
    /**
     * Composer vendor
     */
    $vendor_directory = get_template_directory_uri() . '/vendor/';
    /**
     * Scripts directory
     */
    $js_directory = get_template_directory_uri() . '/js/';
    wp_register_script('jqBootstrapValidation', $js_directory . 'jqBootstrapValidation.js', 'jquery', '3.1.1', true);
    wp_register_script('contact_me', $js_directory . 'contact_me.js', 'jquery', '3.1.1', true);
    wp_register_script('bootstrap', $vendor_directory . 'twbs/bootstrap/dist/js/bootstrap.js', 'jquery', '3.1.1', true);
    wp_register_script('freelancer', $js_directory . 'freelancer.js');
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-form');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('freelancer');

    /**
     * Load our IE specific stylesheet for a range of older versions:
     * <!--[if lt IE 9]> ... <![endif]-->
     * NOTE: You can use the 'less than' or the 'less than or equal to' syntax here interchangeably.
     */
    wp_enqueue_script( 'html5shiv', get_stylesheet_directory_uri() . "/js/IE/html5shiv.js", array(), '3.7.3', false);
    wp_enqueue_script( 'respond', get_stylesheet_directory_uri() . "/js/IE/respond.min.js", array(), '1.4.2', false);
    wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
    wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

/*    if (is_contact()) {
        wp_enqueue_script('jqBootstrapValidation');
        wp_enqueue_script('contact_me   ');
    }*/
}
add_action('wp_enqueue_scripts', 'pf_enqueue_scripts');

/**
 * Fix responsive pictures
 */
function pf_pictures($html) {
    $html = preg_replace('/(width|height)="\d*"\s/', "", $html);
    return $html;
}

add_filter('post_thumbnail_html', 'pf_pictures', 10);
add_filter('image_send_to_editor', 'pf_pictures', 10);
add_filter('wp_get_attachment_link', 'pf_pictures', 10);

/**
 * Add classes nav item
 */
function special_nav_class( $classes, $item ){
    if( is_home() && $item->title == 'Top' ){
        $classes[] = "hidden";
    }
    return $classes;
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function modify_nav_menu_args($args) {
    if('primary' == $args['theme_location'] ) {
        $args['menu_class'] = ' nav navbar-nav navbar-right';
    }
    return $args;
}
add_filter( 'wp_nav_menu_args', 'modify_nav_menu_args' );

/**
 * Add image header
 * @var  $args
 */

$args = array(
    'flex-width'    => true,
    'width'         => 256,
    'flex-height'    => true,
    'height'        => 256,
    'default-image' => get_template_directory_uri() . '/img/profile.png',
);
add_theme_support( 'custom-header', $args );

/**
 * Register the footer widget area
 */
register_sidebar(array(
   'name' => 'Footer sidebar 1',
    'id' => 'footer-sidebar-1',
    'description' => 'Appears in the footer area',
    'before_title' => '<h3>',
    'after_widget' => '</h3>',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
));

register_sidebar(array(
    'name' => 'Footer sidebar 2',
    'id' => 'footer-sidebar-2',
    'description' => 'Appears in the footer area',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Footer sidebar 3',
    'id' => 'footer-sidebar-3',
    'description' => 'Appears in the footer area',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

register_sidebar(array(
    'name' => 'Copyright',
    'id' => 'footer-copyright',
    'description' => 'Appears in the footer area',
    'before_title' => '<h3>',
    'after_title' => '</h3>',

));

// This theme uses wp_nav_menu() in  location.
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'portfolio' ),
    'social'  => __( 'Social Links Menu', 'portfolio' ),
) );

/**
 * Remove title in widget
 */
add_filter( 'widget_title', 'remove_widget_title' );
function remove_widget_title( $widget_title ) {
    if (substr($widget_title, 0, 1 ) == '!') :
        return;
    else:
        return ($widget_title) ;
    endif;
}

/**
 * Custom Menu Social buttons
 */
class description_walker extends Walker_Nav_Menu
{
    /**
     * Starts the element output.
     *
     * @since 3.0.0
     * @since 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
     *
     * @see Walker::start_el()
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param WP_Post $item Menu item data object.
     * @param int $depth Depth of menu item. Used for padding.
     * @param stdClass $args An object of wp_nav_menu() arguments.
     * @param int $id Current item ID.
     */
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = ($depth) ? str_repeat($t, $depth) : '';

        $classes = empty($item->classes) ? array() : (array)$item->classes;
        $classes[] = 'menu-item-' . $item->ID;


        $args = apply_filters('nav_menu_item_args', $args, $item, $depth);


        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args, $depth));
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names . '>';

        $atts = array();
        $atts['title'] = !empty($item->attr_title) ? $item->attr_title : '';
        $atts['target'] = !empty($item->target) ? $item->target : '';
        $atts['rel'] = !empty($item->xfn) ? $item->xfn : '';
        $atts['href'] = !empty($item->url) ? $item->url : '';


        $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);

        $attributes = '';
        foreach ($atts as $attr => $value) {
            if (!empty($value)) {
                $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        /** This filter is documented in wp-includes/post-template.php */
        $title = apply_filters('the_title', $item->title, $item->ID);

        $title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);

        $item_output = $args->before;
        $item_output .= '<a' . $attributes . ' class="btn-social btn-outline">';
        $item_output .= $args->link_before . $title . $args->link_after;
        $item_output .= '<i class="fa fa-fw fa-' . $item->title . '">';
        $item_output .= '</i>';
        $item_output .= '</a>';
        $item_output .= $args->after;


        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}
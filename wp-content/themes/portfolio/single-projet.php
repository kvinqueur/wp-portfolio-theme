<?php
get_header();
?>

<div class="main single">
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <div class="projet">
                <?php if (has_post_thumbnail())
                    the_post_thumbnail('large');
                  ?>
                <h1 class="title">
                    <?php the_title(); ?>
                </h1>
                <div class="content">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>

<?php
get_footer();
?>
